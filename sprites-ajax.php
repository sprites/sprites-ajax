<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://sprites.co
 * @since             1.0.0
 * @package           Sprites_Ajax
 *
 * @wordpress-plugin
 * Plugin Name:       Sprites-Ajax
 * Plugin URI:        sprites-ajax
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Sprites Software s.r.o
 * Author URI:        https://sprites.co
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       sprites-ajax
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'SPRITES_AJAX_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-sprites-ajax-activator.php
 */
function activate_sprites_ajax() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-sprites-ajax-activator.php';
	Sprites_Ajax_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-sprites-ajax-deactivator.php
 */
function deactivate_sprites_ajax() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-sprites-ajax-deactivator.php';
	Sprites_Ajax_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_sprites_ajax' );
register_deactivation_hook( __FILE__, 'deactivate_sprites_ajax' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-sprites-ajax.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_sprites_ajax() {

	$plugin = new Sprites_Ajax();
	$plugin->run();

}
run_sprites_ajax();
