<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://sprites.co
 * @since      1.0.0
 *
 * @package    Sprites_Ajax
 * @subpackage Sprites_Ajax/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
