<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://sprites.co
 * @since      1.0.0
 *
 * @package    Sprites_Ajax
 * @subpackage Sprites_Ajax/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Sprites_Ajax
 * @subpackage Sprites_Ajax/includes
 * @author     Sprites Software s.r.o <hello@sprites.co>
 */
class Sprites_Ajax_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
