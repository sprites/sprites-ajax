<?php

/**
 * Fired during plugin activation
 *
 * @link       https://sprites.co
 * @since      1.0.0
 *
 * @package    Sprites_Ajax
 * @subpackage Sprites_Ajax/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Sprites_Ajax
 * @subpackage Sprites_Ajax/includes
 * @author     Sprites Software s.r.o <hello@sprites.co>
 */
class Sprites_Ajax_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
